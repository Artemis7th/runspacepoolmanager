# Introduction 
This project allows you to queue multiple different script jobs into a runspace pool with a limited number of worker threads. The runspace pool will start jobs until it has filled all of its worker threads and will then hold other jobs until a thread is available. Useful for easy multithreading at the command line and in scripts.
# Getting Started
## Installation
```
Install-Module -Name RunspacePoolManager
```
## How to use
### Starting a Runspace Pool
Starting a runspace pool is easy. By default it will set the max jobs equal to your CPU threads.
```
$runspacePool = Start-RunspacePool
```
You can also set the max threads yourself.
```
$runspacePool = Start-RunspacePool -MaxJobs 5
```
### Submiting a job to a Runspace Pool
Once you have a runspace pool you can submit jobs to it. Currently Start-RunspacePoolJob only supports using variables for the scriptblock.
```
$loopCount = 2147483647
$job = Start-RunspacePoolJob -ScriptBlock {
    $result = 1;
    foreach ($loopnumber in 1 .. $using:loopCount) {
        $result = 1;
        foreach ($number in 1 .. $using:loopCount) {
            $result = $result * $number
        }
        $result
    }
} -RunspacePool $runspacePool
```
### Getting the status of a job
Once you have started a job you can check on its status. IsComplete will be true when the script block is completed.
```
$job
CompletedSynchronously IsCompleted AsyncState AsyncWaitHandle
---------------------- ----------- ---------- ---------------
                 False        True            System.Threading.ManualResetEvent
```
### Stopping a Runspace Pool
Running this command will stop the runspace pool terminating any running jobs and cancelling all pending jobs.
```
Stop-RunspacePool -RunspacePool $runspacePool
```
## Advanced use
### Starting a Syncronized Hash Table
You can create a syncronized hash table and feed it into your jobs to access and modify values across jobs.
```
$HashTable = New-SynchronizedHashTable
```
### Modifying Syncronized Hash Table with cmdlets
You can create keys in the hash table like this:
```
New-HashTableKey -HashTable $HashTable -Key 'test' -Value 'string'
```
You can get the value of a key in the hash table like this:
```
Get-HashTableKey -HashTable $HashTable -Key 'test'
string
```
You can update keys in the hash table by running:
```
Set-HashTableKey -HashTable $HashTable -Key 'test' -Value 'string2'
```
You can remove  keys from the hash table by running:
```
Remove-HashTableKey -HashTable $HashTable -Key 'test'
```
Item types are persisted so you can also add values like this:
```
[int]$test = 243567
Set-HashTableKey -HashTable $HashTable -Key 'test' -Value $test -Force
$(Get-HashTableKey -HashTable $HashTable -Key 'test').GetType()

IsPublic IsSerial Name                                     BaseType
-------- -------- ----                                     --------
True     True     Int32                                    System.ValueType
```
### Notes about the hash table commands
New-HashTableKey and Set-HashTableKey have a -Force parameter for ease of use. Using -Force will figure out that you need to run the other command and run it for you. Therefore it can be slower to use -Force.
Remove-HashTableKey will throw a warning if the key does not exist in the hash table. Using -Force will supress this error.
### Managing hash table directly
While we are talking about speed, these commands are offered as a convienience. It is faster to create and manipulate the object directly.
Creating the runspace:
```
$hashTable = [hashtable]::Synchronized(@{})
```
Adding values:
```
$hashTable.Add('test', 'string')
```
Getting values:
```
$hashTable['test']
string
```
Setting values:
```
[int]$test = 243567
$HashTable['test'] = $test
```
Removing values:
```
$hashTable.Remove('test')
```
### Using a hash table with Start-RunspacePoolJob
You load a hash table into a runspace and you will be able too access, change, add, and remove keys from that hash table.
```
$runspacePool = Start-RunspacePool
[int]$test = 1
$hashTable = New-SynchronizedHashTable
New-HashTableKey -HashTable $HashTable -Key 'test' -Value $test
Start-RunspacePoolJob -RunspacePool $runspacePool -HashTable $HashTable -ScriptBlock {$HashTable['test']++}
Get-HashTableKey -HashTable $HashTable -Key 'test'
2
```
Important: Do not provide a param block when using Start-RunspacePoolJob. The command will manage the param block of the script itself. You must either use a HashTable or $using values in the ScriptBlock.