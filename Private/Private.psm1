﻿function GetUsingVariables {
    param ([scriptblock]$ScriptBlock)
    $ScriptBlock.ast.FindAll({
            $args[0] -is [System.Management.Automation.Language.UsingExpressionAst]
        }, $True)
}

function ConvertScript {
    param (
        [scriptblock]$ScriptBlock,
        [bool]$HasParam,
        $UsingVariables,
        $UsingVariableValues,
        $HashTable
    )
    # $HasParam unused
    $List = New-Object 'System.Collections.Generic.List`1[System.Management.Automation.Language.VariableExpressionAst]'
    $Params = New-Object System.Collections.ArrayList
    if ($UsingVariables) {
        foreach ($Ast in $UsingVariables) {
            [void]$list.Add($Ast.SubExpression)
        }
    }
    if ($UsingVariableValues) {
        [void]$Params.AddRange(@($UsingVariableValues.NewName))
    }
    if ($HashTable) {
        [void]$Params.Add('$HashTable')
    }
    $NewParams = $Params -join ', '
    $Tuple = [Tuple]::Create($list, $NewParams)
    $bindingFlags = [Reflection.BindingFlags]"Default,NonPublic,Instance"
    
    $GetWithInputHandlingForInvokeCommandImpl = ($ScriptBlock.ast.gettype().GetMethod('GetWithInputHandlingForInvokeCommandImpl', $bindingFlags))
    $StringScriptBlock = $GetWithInputHandlingForInvokeCommandImpl.Invoke($ScriptBlock.ast, @($Tuple))
    if ([scriptblock]::Create($StringScriptBlock).ast.endblock[0].statements.extent.text.startswith('$input |')) {
        $StringScriptBlock = $StringScriptBlock -replace '\$Input \|'
    }
    if (-NOT $ScriptBlock.Ast.ParamBlock) {
        $StringScriptBlock = "Param($($NewParams))`n$($StringScriptBlock)"
        [scriptblock]::Create($StringScriptBlock)
    }
    else {
        [scriptblock]::Create($StringScriptBlock)
    }
}
