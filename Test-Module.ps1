﻿#Explicitly import the module for testing
Import-Module 'RunspacePoolManager'

#This test is for massive queueing
$runspacePool = Start-RunspacePool
$i = 0
do {
    $i++
    Start-RunspaceJob -ScriptBlock {
        New-Item -Path "G:\Temp\ProofIRan$using:i.txt"
        Start-Sleep -Seconds 5
    } -RunspacePool $runspacePool
} until ($i -eq 1000)

#This test is for CPU consuption
$runspacePool = Start-RunspacePool
$i = 0
do {
    $i++
    Start-RunspaceJob -ScriptBlock {
        $result = 1;
        foreach ($loopnumber in 1 .. 2147483647) {
            $result = 1;
            foreach ($number in 1 .. 2147483647) {
                $result = $result * $number
            }
            $result
        }
    } -RunspacePool $runspacePool
} until ($i -eq $env:NUMBER_OF_PROCESSORS)
